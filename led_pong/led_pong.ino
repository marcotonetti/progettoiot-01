/*Autori: 
Radicchi Giovanni 789739
Sanchi Piero 793556
Tonetti marco 789483
*/

#include "lib.h"

boolean gamestart; //vero se il gioco è iniziato
boolean flagState;//booleano utilizzato per far lampeggiare il led del vincitore
boolean T;//booleano per non far perdere durante il primo secondo 
int  currIntensity;//attuale intensità del led rosso 
int  fadeAmount;//variabile per regolare la luminosità del led rosso
int direction;//direzione della palla in maniera randomica
int position;//posizione della palla
int switches;//numero di scambi che vanno a influenzare RT
int SPEED;//velocità di movimento della palla regolata tramite potenziometro
int RT; // tempo di reazione

void setup() {
  pinMode(L, OUTPUT); 
  pinMode(L+1, OUTPUT); 
  pinMode(L+2, OUTPUT); 
  pinMode(Lflash, OUTPUT);      
  pinMode(T1, INPUT);  
  pinMode(T2, INPUT);  
  pinMode(T3, INPUT);
  Serial.begin(9600);
  initializer();
  resetGame();
  attachInterrupt(digitalPinToInterrupt(T1), changeDirT1, RISING); 
  attachInterrupt(digitalPinToInterrupt(T2), changeDirT2, RISING);
}

void loop() {
  if(gamestart) {
    noInterrupts();
    T = true;
    interrupts();
      if((L + position)==(L+3)||(L + position)==(L-1)){ //caso 1: fuori dai limiti (ho perso)
        gameWon(position == 3 ? L : L+2);
      }else if((L + position)==(L+2)||(L + position)==(L)){ //caso 2: nei limiti (ho tempo per premere il pulsante)
        digitalWrite(L + position - (direction), LOW); //spengo il pin attualmente acceso
        digitalWrite(L + position, HIGH);//accendo il pin successivo   
        for(int i=0;i<1000;i++){
          delayMicroseconds(RT*pow(7,switches)/pow(8,switches));
        }
        noInterrupts();
          if(gamestart){
           position=position+direction;
          }
        interrupts();
      }else if(L+position==L+1){
        switches=switches+1;
        digitalWrite(L + position - (direction), LOW); //spengo il pin attualmente acceso
        digitalWrite(L + position, HIGH);//accendo il pin successivo   
          for(int i=0;i<1000;i++){
            delayMicroseconds(1000/SPEED);
          }  
        noInterrupts();
          if(gamestart){
            position=position+direction;//aggiorno position alla posizione successiva
          }
        interrupts(); 
      }               
  }else{
    fady(); 
    int value = analogRead(Pot);
    SPEED = map(value, 0, 1023, 1, 5);
    int buttonState = digitalRead(T3);
    delay(40); //impedire il bouncing del bottone 
      if (buttonState){
       digitalWrite(Lflash, LOW); //spegniamo il led rosso (Lflash)                   
       Serial.println("GO!");  
       digitalWrite(L + 1, HIGH);
       delay(1000); 
       randomdirection();
       position += direction;
       gamestart=true; //facciamo partire il gioco
      }
   }
}
