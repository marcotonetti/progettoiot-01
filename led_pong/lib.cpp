#include <Arduino.h>
#include "lib.h"

extern boolean gamestart; //vero se il gioco è iniziato
extern boolean flagState;//booleano utilizzato per far lampeggiare il led del vincitore
extern boolean T;//booleano per non far perdere durante il primo secondo 
extern int  currIntensity;//attuale intensità del led rosso 
extern int  fadeAmount;//variabile per regolare la luminosità del led rosso
extern int direction;//direzione della palla in maniera randomica
extern int position;//posizione della palla
extern int switches;//numero di scambi che vanno a influenzare RT
extern int SPEED;//velocità di movimento della palla regolata tramite potenziometro
extern int RT; // tempo di reazione

void initializer(){
  currIntensity = 0;
  fadeAmount = 15;
  RT = 1000;
}

void fady(){
  analogWrite(Lflash, currIntensity);   
  currIntensity = currIntensity + fadeAmount;
    if (currIntensity == 0 || currIntensity == 255) {
      fadeAmount = -fadeAmount ; 
    }
}

void resetGame(){
  digitalWrite(L, LOW);  
  digitalWrite(L+1, LOW);
  digitalWrite(L+2, LOW); //spegniamo tutti i led di gioco
  Serial.println("Welcome to Led Pong. Press Key T3 to Start");
  gamestart = false;
  position = 1;
  switches = 0;
  T = false;
  flagState = false;
}

void changeDirT1(){ 
    if(T==true){
    static int last_interrupt1 = 0;
    int time1 = millis();
      if(time1 - last_interrupt1 > 200){
        if(L+position==L){
          direction*=-1;//direction invertita
        }else if(gamestart){
          gameWon(L+2);
        }
      }
    last_interrupt1 = time1;
    }
}

void changeDirT2(){
  if(T==true){
    static int last_interrupt2 = 0;
    int time2 = millis(); 
    if(time2 - last_interrupt2 > 200){
      if(L+position==L+2){
       direction*=-1;//direction invertita
      } else if(gamestart){
       gameWon(L);
        }
      }
    last_interrupt2 = time2;
  }
}

void gameWon(int winner){  //il led del vincitore lampeggia per due secondi
    T = false;
    int win = winner == L ? 1 : 2;
    Serial.print("Game Over - The Winner is Player ");
    Serial.print(win);
    Serial.print(" after ");
    Serial.print(switches);
    Serial.println(" shots.");
    resetGame();
    for(int i = 0; i<8; i++){
      if (!flagState){
        digitalWrite(winner, HIGH);  
      } else {
        digitalWrite(winner, LOW);  
      }
      flagState = !flagState;
      for(int i = 0; i<20; i++){
        delayMicroseconds(12500);
      }
    }
}

void randomdirection(){
 int randomnumber = (millis()%2);
  if(randomnumber == 0){
    direction = -1;
  }else{
    direction = 1;
  }
}
