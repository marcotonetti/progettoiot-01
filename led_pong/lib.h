#ifndef __LIB_H__

#define __LIB_H__

#define Pot A0 //pot_pin
#define T1 2  //button_pin 1
#define T2 3  //button_pin 2
#define T3 13 //button_pin start
#define L 4 //led_pin 
#define Lflash 9  //red_pin

void initializer();
void resetGame();
void fady();
void changeDirT1();
void changeDirT2();
void gameWon(int winner);
void randomdirection();

#endif
